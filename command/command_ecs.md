
## Script: ./ecs

This command is used for deploying ecs services.

### Usage

```
./ecs {{project_name}} {{project_env}} <cmd> <service-arg>
```

### List of supported commands.

cmd | comment
--- | ---
taskdef | Creates service taskdefinition in ECS
service | Creates and starts the service
start | same as service cmd
deploy | Combines taskdef and service command into one.
update | Updates the service.
stop | Stops the service.
restart | Restarts the service.
stop | stop the service.
stop-rm | stop and then remove the service.
check-rm | check for the service whether it is stopped then stop the service.
rm | remove the service if its already stopped.
autoscale | autoscale the service
autoscale-destroy | remove/destroy autoscale for the service.
autoscale-rm | same as autoscale-destroy
destroy | undeploy the service (stops, removes autoscale and then removes the service from the cluster)
cfg | update config for the service.

### List of supported <service-arg>

arg | detail | comment
--- | --- | ---
service-name... | one or many service-name | see ops/services/all.list, ui.list, infra.list content for supported service names.
service-list-files | path to list file | see ops/services/all.list for file format.
all | - | see ops/services/all.list (this command uses this list file)
infra | - | see ops/services/infra.list (this command uses this list file)
ui | - | see ops/services/ui.list (this command uses this list file)

### Example Usage

```
./ecs {{project_name}} {{project_env}} taskdef eventhub configuration
./ecs {{project_name}} {{project_env}} taskdef all
./ecs {{project_name}} {{project_env}} deploy /path/to/services.list
```
