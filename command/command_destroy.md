
## Script: ./destroy

This command consolidates all subcommands to destroy the whole cluster and its dependent resources completely.

### Usage

```
./destroy {{project_name}} {{project_env}}
```
