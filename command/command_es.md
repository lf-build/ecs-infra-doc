
## Script: ./es

This script connects with elasticsearch for log management.

### Usage

```
./es {{project_name}} {{project_env}} <cmd> <cmd-args>
```

### List of supported commands

cmd | comment
--- | ---
logs/tail | show / tail logs
indexes | show list of indexes in the elasticsearch
cleanup  | delete old logs (default is to remove 2 weeks old logs)
archive  | Automatically downloads the logs as tar.gz and archives/syncs to s3 and then deletes them from elasticsearch.
export   | Export logs and stores as tar.gz locally.
sync2s3  | Syncs the exported logs to s3 for public access.

### Dependencies

package | depended cmd | install
--- | --- | ---
elktail | logs | Install elktail
elasticsearch-curator | indexes,cleanup |  https://www.elastic.co/guide/en/elasticsearch/client/curator/5.1/installation.html

### List of supported <cmd-args>

cmd | args
--- | ---
logs | see elktail help (--url, -f, -i, -t cannot be used in this command - rest of the options are supported.)
indexes | <none>
cleanup  | <none>
archive  | <size_in_gb> (this arg is optional - default is 31)
export  | <name/query> <fromdate> <todate> (todate is optional)
sync2s3  | <none>

### Example Usage

```
./es {{project_name}} {{project_env}} logs
./es {{project_name}} {{project_env}} logs source=stderr
./es {{project_name}} {{project_env}} logs -a "2016-06-17T15:00" source=stderr

./es {{project_name}} {{project_env}} indexes
./es {{project_name}} {{project_env}} cleanup

./es {{project_name}} {{project_env}} archive
./es {{project_name}} {{project_env}} export - -a "2016-06-17T15:00" source=stderr
./es {{project_name}} {{project_env}} sync2s3
```

### Command: logs

- logs command fetches logs from elasticsearch and shows in the console using elktail
- Download and install elktail before using this command.

##### elktail help (for reference)

```
elktail -h
NAME:
   elktail - utility for tailing Logstash logs stored in ElasticSearch

USAGE:
   elktail [global options] [query-string]
   Options marked with (*) are saved between invocations of the command. Each time you specify an option marked with (*) previously stored settings are erased.

VERSION:
   0.1.7

GLOBAL OPTIONS:
   --url value                        (*) ElasticSearch URL (default: "http://127.0.0.1:9200")
   -f value, --format value           (*) Message format for the entries - field names are referenced using % sign, for example '%@timestamp %message' (default: "%message")
   -i value, --index-pattern value    (*) Index pattern - elktail will attempt to tail only the latest of logstash's indexes matched by the pattern (default: "logstash-[0-9].*")
   -t value, --timestamp-field value  (*) Timestamp field name used for tailing entries (default: "@timestamp")
   -l, --list-only                    Just list the results once, do not follow
   -n value                           Number of entries fetched initially (default: 50)
   -a value, --after value            List results after specified date (example: -a "2016-06-17T15:00")
   -b value, --before value           List results before specified date (example: -b "2016-06-17T15:00")
   -s                                 Save query terms - next invocation of elktail (without parameters) will use saved query terms. Any additional terms specified will be applied with AND operator to saved terms
   -u value                           (*) Username for http basic auth, password is supplied over password prompt
   --ssh value, --ssh-tunnel value    (*) Use ssh tunnel to connect. Format for the argument is [localport:][user@]sshhost.tld[:sshport]
   --v1                               Enable verbose output (for debugging)
   --v2                               Enable even more verbose output (for debugging)
   --v3                               Same as v2 but also trace requests and responses (for debugging)
   --version, -v                      Print the version
   --help, -h                         Show help
```

### Command: cleanup

This command removes old logs/indexes from elasticsearch. By default it will remove 2 weeks older logs.

Some of the environment variables that can be overridden are as follows.

env | default | comment
--- | --- | ---
ES_UNIT | weeks | units in days weeks months years etc..
ES_UNIT_VALUE | 2 | the unit value.
RUN_CURATOR | "" | set this variable to `y` to run cleanup command because cleanup command will dryrun by default.

##### Example `cleanup` commands

```
./es {{project_name}} {{project_env}} cleanup
ES_UNIT=days ES_UNIT_VALUE=7 RUN_CURATOR=y ./es {{project_name}} {{project_env}} cleanup
```

### Command: archive

This command archives the logs based on elasticsearch index usages and stores them in s3 and also deletes them from elasticsearch for free the space.

Some of the environment variables that can be overridden are as follows.

env | default | comment
--- | --- | ---
ES_EXPORT_ARCHIVE_PATH | logs/archive | files in this folder gets syncs to s3
ES_EXPORT_ARCHIVE_S3_REGION | <none> | S3 bucket region where the logs archive files needs to be synced. (mandatory)
ES_EXPORT_ARCHIVE_S3_BUCKET | <none> | S3 bucket where the logs archive files needs to be synced. (mandatory)
ES2CSV_PATH | es2csv | you can override the location of es2csv binary using this environment variable.
FORCE_EXPORT | <none> | Mostly used while running the command to force export (meaning, it will delete the already exported files if there is any before starting new export)

The location where the logs gets archived to s3 is `s3://${ES_EXPORT_ARCHIVE_S3_REGION}/${PROJECT}-archive/``

##### Example `archive` commands

```
./es {{project_name}} {{project_env}} archive

# if you want to limit the size of final elasicsearch index size for example in this 30G, run the following command.
./es {{project_name}} {{project_env}} archive 30

FORCE_EXPORT=y ./es {{project_name}} {{project_env}} archive 31
```

### Command: export

This command exports the logs from elasticsearch and stores them locally in compressed format.

##### Usage

```
./es {{project_name}} {{project_env}} <name/query/-> <fromdate> <todate>
```

##### List of supported <cmd-args>

argno | argname | comment
--- | ---
1 | name | First argument supports service name.
1 | query | First argument also supports simple queries but its not recommended use stdin.
1 | - | First argument also supports `-` which is read query from stdin. i.e query can be supplied through stdin.
2 | fromdate (format: d[ymd])| 2nd argument supports simple formats like 1d, 1m, 1y
2 | fromdate (format: yyyy-mm-dd)| 2nd argument supports yyyy-mm-dd format also for from date.
3 | todate (format: yyyy-mm-dd)| 3rd argument todate is optional and supports only one format yyyy-mm-dd

##### Example `export` commands

```
./es {{project_name}} {{project_env}} export configuration 1d
./es {{project_name}} {{project_env}} export configuration 2017-08-10 2017-08-11
echo "source = 'stderr' and container_name = 'configuration'" | ./es {{project_name}} {{project_env}} export - 1d
```

### Command: sync2s3

This command syncs the exported logs to s3.

Some of the environment variables that can be overridden are as follows.

env | default | comment
--- | --- | ---
ES_EXPORT_SHARE_PATH | logs/share | files in this folder gets syncs to s3
ES_EXPORT_SHARE_S3_REGION | <none> | S3 bucket region where the logs files needs to be synced. (mandatory)
ES_EXPORT_SHARE_S3_BUCKET | <none> | S3 bucket where the logs files needs to be synced. (mandatory)

The location where the logs gets synced to s3 is `s3://${ES_EXPORT_SHARE_S3_BUCKET}/${PROJECT}-logs/``
