
## Script: ./maint

This command is wrapper on top of tf script to start/stop maintenance mode

### Usage

```
./maint {{project_name}} {{project_env}} <cmd> <sub-cmd>
```

### List of supported commands.

cmd | comment
--- | ---
on | Puts the cluster in maintenance mode.
off | Reverts back the cluster to normal mode.

### List of supported sub commands.

service | comment
--- | ---
plan | similar to tf plan as ./maint internally calls tf
apply| similar to tf apply as ./maint internally calls tf

### Example Usage

```
./tf {{project_name}} {{project_env}} on plan
./tf {{project_name}} {{project_env}} on apply
./tf {{project_name}} {{project_env}} off plan
./tf {{project_name}} {{project_env}} off apply
```

**Note:** Always run plan and then apply similar to ./tf script. plan will show you what infra changes are going to be done.
