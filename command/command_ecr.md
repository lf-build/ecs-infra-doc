
## Script: ./ecr

This command is used for working with ECR.

### Usage

```
./ecr {{project_name}} {{project_env}} <cmd> <cmd-args-if-any>
```

### List of supported commands.

cmd | comment
--- | ---
auth | authenticate with ECR
compose-push | push all the images in docker-compose file to ECR.
compare-env  | lists docker compose env and task definition environment variables for comparision.
rm | removes the ECR repository.

### Example Usage

```
./ecr {{project_name}} {{project_env}} auth
./ecr {{project_name}} {{project_env}} compose-push /path/to/docker-compose.yml {{image-name mentioned in docker-compose file}}
./ecr {{project_name}} {{project_env}} compare-env /path/to/docker-compose.yml
./ecr {{project_name}} {{project_env}} rm [path/to/listfile or service-name...]
```
