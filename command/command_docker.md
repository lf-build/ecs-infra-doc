
## Script: ./docker

This command is used for building and pushing docker images to ECR docker registry.

### Usage

```
./docker {{project_name}} {{project_env}} <cmd> <servicename>
```

### List of supported commands.

cmd | comment
--- | ---
build | build docker image
auth | authenticate with ECR
repo | create repo for the service in ECR
push | push image to ECR.

### List of supported services.

service | comment
--- | ---
traefik-ecs | api interface for accessing all services in ecs
https-redirect | redirects http endpoint to https.
traefik-ui | exposes traefik process UI in traefik-ecs service
redirect | redirects to any configured endpoint

**Note:** See ecs-infra/dockerfiles folder.

### List of unsupported (notused anymore) services.

service | comment
--- | ---
traefik-es | a simple proxy to forward all requests to Amazon Elastic search.
fluentd-es | sends logs to traefik-es container service

**Note:** See ecs-infra/dockerfiles folder.

### Example Usage

```
./docker {{project_name}} {{project_env}} build traefik-ecs
./docker {{project_name}} {{project_env}} auth traefik-ecs
./docker {{project_name}} {{project_env}} repo traefik-ecs
./docker {{project_name}} {{project_env}} push traefik-ecs
```
