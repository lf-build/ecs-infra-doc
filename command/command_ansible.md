
## Script: ./ansible

This command is mainly used for operating or managing the ecs cluster once it is provisioned.

### Usage

```
./ansible {{project_name}} {{project_env}} <cmd> <tag>
```

### List of supported cmd + tag

cmd | tag
--- | ---
docker | restart
docker | clean
ecs-agent | restart
ecs-agent | start
ecs-agent | info
fluentd | restart
fluentd | start
weave | restart
weave | start
weave-scope | restart
weave-scope | start

### Example Usage

```
./ansible {{project_name}} {{project_env}} docker clean
./ansible {{project_name}} {{project_env}} weave-scope restart
./ansible {{project_name}} {{project_env}} ecs-agent info
```
