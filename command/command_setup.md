
## Script: ./setup

This command is wrapper on top of ansible script to use ecs-ansible startup scripts.

Download/clone the ecs-ansible repo before using this command as shown in the folder structure below.

```

 parent-folder
   |
   |-- ecs-infra
   |
   |-- ecs-ansible

```

### Usage

```
./setup {{project_name}} {{project_env}}
```
