
## Script: ./tf

This command is used for provisioning infrastructure.

### Usage

```
./ecr {{project_name}} {{project_env}} <project_infra_module> <cmd>
```

### List of supported infra module

project_infra | comment
--- | ---
vpc | Creates vpc environment.
bastion | Creates a bastion box on public subnet.
ecs-weave | Creates a ecs cluster on private subnet.

### List of supported commands

cmd | comment
--- | ---
init | Initialize the terraform module and state storage location.
plan | Do terraform plan for the module.
apply | Do terraform apply for the module.
destroy | Do terraform destroy for the module.
out | List the output variable values for the module.

### Example Usage

```
./tf {{project_name}} {{project_env}} vpc init
./tf {{project_name}} {{project_env}} bastion plan
./tf {{project_name}} {{project_env}} ecs-weave apply
./tf {{project_name}} {{project_env}} ecs-weave out
```
