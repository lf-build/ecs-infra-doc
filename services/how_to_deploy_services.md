

- vars/{{project_name}}/ecs.vars

```
export DOCKER_REGISTRY=???
```

- vars/{{project_name}}-{{project_env}}/ecs.vars

```
export DOCKER_LOGDRIVER=fluentd

export MONGODB_USERPASS="ce-admin:S1gmA@"
export MONGODB_URI="mongodb://${MONGODB_USERPASS}?????"
export MONGODB_REPLICASET="???"
export DEFAULT_MONGODB_URI="${MONGODB_URI}/admin?replicaSet=${MONGODB_REPLICASET}"
export ADMIN_MONGODB_URI=$DEFAULT_MONGODB_URI
export APP_MONGODB_URI=$DEFAULT_MONGODB_URI

export NATS_URL="nats://nats:4222"

export REDIS_HOST="????"
export REDIS_PORT="18576"
export REDIS_URI="${REDIS_HOST}:${REDIS_PORT}?db=1&connectiontimeout=500"

export TRAEFIK_ECS_REFRESHSECONDS=120

export PORTAL_TOKEN="????"
export WORKFLOW_MONGODB_URI="${MONGODB_URI}/workflow?replicaSet=${MONGODB_REPLICASET}&authSource=admin"
export WORKFLOW_REDIS_URI="redis://${REDIS_HOST}:${REDIS_PORT}"
export TENANT_NAME="ce-uat"

export BORROWER_PORTAL_URL="https://apply.portal.qbera.com"
export WORKFLOW_URL="https://workflow.portal.qbera.com"
export SERVICES_HOST="services.api.qbera.com:9090"
export SERVICES_URL="http://${SERVICES_HOST}"
```
