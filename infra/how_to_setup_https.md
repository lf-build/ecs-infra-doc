
## How to setup https?

Import domain certs to AWS Certificate Manager and note down the cert arn id for each domain/subdomain.

Add the following informations in the vars/qbera-{{project_env}}/terraform.tfvars file.

```
https_enabled = true

ssl_cert_arn = ""

# If each service has different domain, then it can overriden with the following cert settings.
# If all services uses same subdomain, then these are not needed, only ssl_cert_arn can be used.
weavescope_ssl_cert_arn = ""
services_ssl_cert_arn = ""
bankportal_ssl_cert_arn = ""
backoffice_ssl_cert_arn = ""
ui_ssl_cert_arn = ""
app_ssl_cert_arn = ""
verify_ssl_cert_arn = ""

```

and reapply ecs-weave infra module.

```
./tf {{project_name}} {{project_env}} ecs-weave plan
./tf {{project_name}} {{project_env}} ecs-weave apply
```
