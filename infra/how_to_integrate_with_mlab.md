
## How to integrate with mlab?

Follow the instructions as per the link http://docs.mlab.com/private-environments/#peering (ignore point 9, as it can be done using the below script)

Add the following informations in the vars/qbera-{{project_shared_env}}/vpc/terraform.tfvars file.

```
vpc_peering_route_enabled = true
vpc_peering_id = "{{mlab_vpc_peering_id}}"
```

and reapply vpc module.

```
./tf {{project_name}} {{project_shared_env}} vpc plan
./tf {{project_name}} {{project_shared_env}} vpc apply
```
