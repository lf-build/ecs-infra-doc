

### Define variables

```
region=ap-southeast-1
region_short=apsoutheast1
az_count=2
project_name=qbera
project_shared_env=share
project_env=prod
tf_state_s3_bucket=sigma-infra-apsoutheast1
```

### Prerequisites

- Create keypair/s in that {{region}}

If you prefer to use single key to login to bastion box and all ecs env instances
then following this convention.

```
key-{{project_name}}-default-{{region_short}}
```

else create keys as shown below.

```
key-{{project_name}}-{{project_shared_env}}-{{region_short}}  -> used in bastion
key-{{project_name}}-{{project_env}}-{{region_short}} -> used in ECS instances for that environment
```

- Create an S3 bucket in {{region}}

```
sigma-infra-{{region_short}}
```

### Create vars folder.

```
vars/{{project_name}}
vars/{{project_name}}-{{project_shared_env}}
vars/{{project_name}}-{{project_shared_env}}/vpc
vars/{{project_name}}-{{project_shared_env}}/bastion
vars/{{project_name}}-{{project_env}}
```

### Existing variables

```
vars/main.vars
vars/terraform.tfvars
vars/ecs.vars
vars/docker.vars
vars/ansible.vars
```

### Setup variables

- vars/{{project_name}}/main.vars

```
export AWS_PROFILE=ce-ecs
export AWS_REGION={{region}}
export TF_VAR_terraform_state_s3_bucket={{tf_state_s3_bucket}}

# TF Variables
export TF_VAR_aws_region=${AWS_REGION}
export TF_VAR_aws_profile=${AWS_PROFILE}
```

- vars/{{project_name}}/terraform.tfvars

```
ecs_ansible_region = "us-east-1"
ecs_ansible_path = "s3://ce-src-useast1/ecs-ansible"
```

- vars/{{project_name}}-{{project_shared_env}}/vpc/terraform.tfvars

```
vpc_cidr = "10.10.0.0/16"
az_count = {{az_count}}
```

- vars/{{project_name}}-{{project_shared_env}}/bastion/terraform.tfvars

```
instance_type = "t2.micro"
```

- vars/{{project_name}}-{{project_env}}/main.vars

```
export AWS_KEYNAME=key-${TF_VAR_project_name}-??? (use the same key that is created in this tuto)
export TF_VAR_key_name=${AWS_KEYNAME}
```


### Create VPC Infra.

```
./tf {{project_name}} {{project_shared_env}} vpc init
./tf {{project_name}} {{project_shared_env}} vpc plan
./tf {{project_name}} {{project_shared_env}} vpc apply
```

### Create Bastion Infra.

```
./tf {{project_name}} {{project_shared_env}} bastion init
./tf {{project_name}} {{project_shared_env}} bastion plan
./tf {{project_name}} {{project_shared_env}} bastion apply
```

### Setup ecs infra variables

- vars/{{project_name}}-{{project_env}}/terraform.tfvars

```

instance_type = "m4.4xlarge"
asg_min = 1
asg_desired = 1
asg_max = 1

root_disk_size = 100??
alrm_ecs_memr_thresold = 85
asg_memr_high_thresold = 85

user_data_type = "custom1"

http_enabled = true
https_enabled = false
```

### Create ECS Infra.

```
./tf {{project_name}} {{project_shared_env}} ecs-weave init
./tf {{project_name}} {{project_shared_env}} ecs-weave plan
./tf {{project_name}} {{project_shared_env}} ecs-weave apply
```
