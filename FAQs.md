## What version of terraform is supported ?

The terraform scripts are heavily tested against terraform version v0.8.8

```
terraform -v
Terraform v0.8.8
```

## What version of ansible is supported?

The ansible scripts are heavily tested against ansible version 2.2.1.0

```
ansible --version
ansible 2.2.1.0
```

## What is tf script?

./tf script is a wrapper around terraform executable.
It helps in setting some variables and other things based on the parameters passed.
see ./tf script for more information.

```
./tf <project_name> <project_env> <infra_module> <cmd>
```

## What are the infrastructure modules?

The infrastructure modules are reusable components/modules that can
be reused multiple times. Each module is responsible for provision a piece of infrastructure. Note: Some module may depend on another.

The modules are...

- VPC
- bastion
- es
- ecs-weave

Points to Remember:

- VPC is the base module which provisions vpc infrastructure.
- bastion module is responsible for provisioning bastion node for external access and it depends on VPC module. i.e) VPC infrastructure has to be provisioned before provisioning bastion infrastructure.
- es module is responsible for provisioning amazon elastic search and it also depends on VPC module. So, VPC infrastructure has to be provisioned before provisioning es infrastructure.
- ecs-weave module is responsible for provisioning ecs cluster and it also depends on VPC module.

To make it simple all the common modules are provisioned under shared environment.
meaning... VPC, bastion, es are also provisioned under project_env=share
because these can be reused for multiple ecs cluster environments.

For example:
1)
project_name=ce-ecs
project_env=share
infra_module=vpc

vpc is provised under environment name "share"

similarly
project_name=ce-ecs
project_env=share
infra_module=bastion
&
project_name=ce-ecs
project_env=share
infra_module=es

bastion and es infrastructure are also provisioned under environment name "share"

but ecs-weave infrastructure (i.e ecs cluster) can be provisioned for multiple environments reusing the underlying same VPC infrastructure

For example:
`project_name=ce-ecs`
`project_env=dev1`
`infra_module=ecs-weave`
&
`project_name=ce-ecs`
`project_env=qa1`
`infra_module=ecs-weave`

## How to create base infrastructure?

To create VPC, follow the commands.

```
./tf ce-ecs share vpc init
./tf ce-ecs share vpc plan
./tf ce-ecs share vpc apply
```

To create bastion box, follow the commands

```
./tf ce-ecs share bastion init
./tf ce-ecs share bastion plan
./tf ce-ecs share bastion apply
```

To create ElasticSearch service, follow the commands

```
./tf ce-ecs share es init
./tf ce-ecs share es plan
./tf ce-ecs share es apply
```

## How to create ecs cluster infrastructure?

For example:
if
project_name=ce-ecs
project_env=dev1
infra_module=ecs-weave
&
project_name=ce-ecs
project_env=qa1
infra_module=ecs-weave

use the following commands to initialize and create the infrastructure.

```
./tf ce-ecs dev1 ecs-weave init
./tf ce-ecs dev1 ecs-weave plan
./tf ce-ecs dev1 ecs-weave apply
```

```
./tf ce-ecs qa1 ecs-weave init
./tf ce-ecs qa1 ecs-weave plan
./tf ce-ecs qa1 ecs-weave apply
```

## How to update ecs cluster infrastructure changes?

To update the infrastructure, terraform apply command has to be executed. There is a simple script tf which is wrapper on top of terraform cli.
Before updating an infrastructure, we need to check what changes are going to be applied by running the plan command as shown below.

```
./tf ce-ecs qa1 ecs-weave plan
```

Once we are satisfied with the command output, we can apply the changes by running the apply command as shown below.

```
./tf ce-ecs qa1 ecs-weave apply
```

## How to destroy part of infrastructure?

To destroy the infrastructure, use the destroy command as shown below.

For example:

If
`project_name=ce-ecs`
& `project_env=qa1`
& `infra_module=es`

To destroy elasticsearch infrastructure the command would be
```
./tf ce-ecs qa1 es destroy
```

### Troubleshooting apply
LaunchConfiguration Issue: There is a known issue / limitation on terraform to delete and recreate same launch configuration name with different configuration options. In that case, if apply gives error deleting and recreating a launch configuration, we need to manually login to the AWS console and delete the launch configuration and run the apply command again.

## How to change the instance type of the cluster?

Change the instance type for the variable instance_type at `var/${project_name}-${project_env}/terraform.tfvars`

For example:
If `project_name=ce-ecs`
& `project_env=qa1`
& `instance_type = "m4.4xlarge"``

Open `var/ce-ecs-qa1/terraform.tfvars` and change `instance_type = "m4.4xlarge"``

And Update the infrastructure (see How to update infrastructure changes?)

**Warning:** This terminates all existing instances and recreates new instances.

## How to scale up or scale down the cluster?

Change the auto scaling variables (asg_min, asg_max, asg_desired) at `var/${project_name}-${project_env}/terraform.tfvars`

asg_min = minimum nodes that should be running in the cluster
asg_max = maximum nodes that can be running in the cluster
asg_desired = number of nodes that currently/should-be running in the cluster (asg_desired should be between asg_min and asg_max)

For example:
If `project_name=ce-ecs`
& `project_env=qa1`
& `asg_desired = 1`

Open `var/ce-ecs-qa1/terraform.tfvars` and change `asg_desired = 1`

And Update the infrastructure (see How to update infrastructure changes?)

## How to enable/disable http endpoints for the cluster?

Set the `http_enabled=false` at `var/${project_name}-${project_env}/terraform.tfvars`

For example:
If `project_name=ce-ecs`
& `project_env=qa1`
& `http_enabled=false`

Open `var/ce-ecs-qa1/terraform.tfvars` and change `http_enabled = false`

And Update the infrastructure (see How to update infrastructure changes?)
