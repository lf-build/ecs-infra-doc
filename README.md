


## Prerequisite

#### Install Tools and Configure
1.  aws-cli
2.  terraform - https://www.terraform.io/downloads.html
3.  jq - https://stedolan.github.io/jq/download/


Configure aws-cli using `aws configure` and create profiles for the project.

#### Setup ecs-infra

##### Prerequisite


-   Get / download the src.
-   Uncompress it in a folder

##### Vars Explained

There are two types of files to configure variables.
- terraform.tfvars
this is terraform variables file.
- vars.env
this is similar to script file just do export VAR=value in this file.

Some terraform variables can be set using environment variable - just prefix the variable with TF_VAR_

Some variables has to be set as environment variables because they are used by the ./ecs and ./tf scripts as well. They are...

```
TF_VAR_aws_profile
TF_VAR_aws_region
TF_VAR_terraform_state_s3_bucket
```

These env variables are used by ./ecs script.

```
DOCKER_REGISTRY
DOCKER_TAG
```

Here are the list of variables that may be need to setup infra.

```
aws_region
aws_profile
project_name
project_env
key_name
vpc_cidr
az_count
log_retention_in_days
terraform_state_s3_bucket
```

#### Vars Folder Structure

./tf script looks for variables in vars.env and terraform.tfvars files in the following order

```
vars/
vars/${project_name}-${project_env}/
vars/${project_name}-${project_env}/${infra_module}/
```

you can override the variable in each level and the last level takes the highest precedence.
better to move all common variables in the higher level folders.

i.e
- place most common variable in vars/
- or place project env common variables in vars/${project_name}-${project_env}/
- or place infra module specific variables in vars/${project_name}-${project_env}/${infra_module}/


Sample vars folder structure -- shown below.

![Screen Shot 2017-03-12 at 11.23.36
PM.png](images/image03.png)

### Infra Design

The infrastructure provisioning is split into 3 parts
- VPC
- Bastion
- ECS

##### Assumptions
- Project may need many environment - dev, test, prod
- Project uses same VPC for all env.
- Bastion is common for all env.

e.g. lets consider these values for this example/tutorial.

```
project_name = ce-ecs
project_env = dev1
key_name = key-ce-ecs-dev-useast1
aws_region = us-east-1
aws_profile = ce-ecs
terraform_state_s3_bucket = infra-useast1

```

- Make sure ec2 key key-ce-ecs-dev-useast1 has been created.
- Make sure aws cli is configured with profile ce-ecs - Also the profile should have access to all AWS Services that's needed to provision the infrastructure.
- Make sure a s3 bucket infra-useast1 is created with version enabled - This is to store terraform infra state files.

## Provision Infrastructure

- Setup Common Vars
![Screen Shot 2017-03-12 at 11.40.06
PM.png](images/image13.png)

### Provision VPC

- Setup VPC Specific Vars for the project
![Screen Shot 2017-03-12 at 11.43.29
PM.png](images/image09.png)
  - The folder structure has VPC env name and its named as share because it's shared by all env.
  - Two variables are set here - vpc_cidr and az_count

- Init VPC module
![Screen Shot 2017-03-12 at 11.46.32
PM.png](images/image05.png)

- Plan VPC Infra and Check resources to be created!
```
./tf ce-ecs share vpc plan
```

- Provision VPC Infra
```
./tf ce-ecs share vpc apply
```

### Provision Bastion

- Setup Bastion Specific Vars for the project
![Screen Shot 2017-03-12 at 11.52.21
PM.png](images/image08.png)
  - Bastion depends on vpc infrastructure and hence it needs to know where to fetch the vpc state file from s3 and use it for reading the vpc variables.
  - Bastion instance type is set to t2.micro


- Init Bastion module
![Screen Shot 2017-03-12 at 11.51.24
PM.png](images/image07.png)

- Plan Bastion Infra and Check resources to be created!
```
./tf ce-ecs share bastion plan
```
![Screen Shot 2017-03-13 at 12.00.13
AM.png](images/image02.png)

- Provision Bastion Infra
```
./tf ce-ecs share bastion apply
```
![Screen Shot 2017-03-12 at 11.56.45
PM.png](images/image00.png)

##### Notes

- Here the resources are already created hence no changes applied.

### Provision ECS

- Setup ECS Specific Vars for the project
![Screen Shot 2017-03-13 at 12.00.57
AM.png](images/image04.png)

- Plan ECS Infra and Check resources to be created!
```
./tf ce-ecs dev1 ecs-weave plan
```

- Provision ECS Infra
```
./tf ce-ecs dev1 ecs-weave apply
```
![Screen Shot 2017-03-13 at 12.02.49
AM.png](images/image11.png)
![Screen Shot 2017-03-13 at 12.03.35
AM.png](images/image06.png)

## Deploy Docker Services

### Define Tasks

- To define tasks see the e.g. commands below.
![Screen Shot 2017-03-13 at 12.07.51
AM.png](images/image12.png)
```
./ecs ce-ecs dev1 taskdef redis
./ecs ce-ecs dev1 taskdef nats
./ecs ce-ecs dev1 taskdef eventhub
./ecs ce-ecs dev1 taskdef configuration
```

##### Notes

-   taskdef cmd has to be done only when there is any change in the
    task/container definition json or for the first time.

### Launch Services

- Launch a service with ELB enabled. Use the option -t to pass the target group arn as shown below.
```
/ecs ce-ecs dev1 service configuration -p 5000 -t arn:aws:elasticloadbalancing:us-east-1:xxxx:targetgroup/z-albxxx/b9a35ec860390657
```

- Launch a service without ELB.
![Screen Shot 2017-03-13 at 12.04.21
AM.png](images/image01.png)
```
./ecs ce-ecs dev1 service redis
./ecs ce-ecs dev1 service nats
./ecs ce-ecs dev1 service eventhub
./ecs ce-ecs dev1 service configuration
```

- Scale the service
```
# pass the desired count as shown below.
./ecs ce-ecs dev1 service configuration 3
```



### Weave UI

- Weave UI shows all the services launched
![Screen Shot 2017-03-13 at 12.09.12
AM.png](images/image10.png)
